package main

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
)

const (
	// Вписываем данные базы данных
	HOST     = "ec2-176-34-114-78.eu-west-1.compute.amazonaws.com"
	DATABASE = "d6ntt0if8oo6ql"
	USER     = "ehsstplrzbvdma"
	PASSWORD = "3cc9a55987e981be6f8e045bdac61807f009d97b5c0fcf488077d41e62030413"
)

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	// Подключаемся к серверу
	var connectionString string = fmt.Sprintf("host=%s user=%s password=%s dbname=%s sslmode=require", HOST, USER, PASSWORD, DATABASE)

	// Подключаемся к базе данных
	db, err := sql.Open("postgres", connectionString)
	checkError(err)

	err = db.Ping()
	checkError(err)
	fmt.Println("Successfully created connection to database")

	// Избавляемся от таблицы, если таковая таблица существует
	_, err = db.Exec("DROP TABLE IF EXISTS inventory;")
	checkError(err)
	fmt.Println("Finished dropping table (if existed)")

	// Создаём таблицу
	_, err = db.Exec("CREATE TABLE inventory (id serial PRIMARY KEY, name VARCHAR(50), quantity INTEGER);")
	checkError(err)
	fmt.Println("Finished creating table")

	// Добавляем информацию в таблицу
	sql_statement := "INSERT INTO inventory (name, quantity) VALUES ($1, $2);"
	_, err = db.Exec(sql_statement, "banana", 150)
	checkError(err)
	_, err = db.Exec(sql_statement, "orange", 154)
	checkError(err)
	_, err = db.Exec(sql_statement, "apple", 100)
	checkError(err)
	fmt.Println("Inserted 3 rows of data")
}
